# FlyChat
## What is FlyChat?
  FlyChat is messaging app for iOS and Android. FlyChat allows to connect with people, no matter who they are, or where they from. 
### Main capabilities:
- View and edit user profile
- Send a message
- Share a photo, sticker or any other file
- Send a voice message
- Start a group chat
- Notifications
- No ads
### Core technologies:
* Realm - database. https://realm.io
### Platform technologies:
* Swift - iOS programming language
* Kotlin - Android programming language
#### Developers
* Kozachenko V.V.
* Kirianov V.O.